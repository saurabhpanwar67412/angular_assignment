import { Component, OnInit } from '@angular/core';
import {ComponentconfigComponent} from '../componentconfig/componentconfig.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';



@Component({
  selector: 'app-childcomponent',
  templateUrl: './childcomponent.component.html',
  styleUrls: ['./childcomponent.component.scss']
})
export class ChildcomponentComponent implements OnInit {
  public unique_key: number;
  public parentRef: ComponentconfigComponent;

  constructor(public matDialog: MatDialog) { }
  

  ngOnInit(): void {
  }


  remove_me() {
    console.log(this.unique_key)
    this.parentRef.remove(this.unique_key)
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }
  


}

