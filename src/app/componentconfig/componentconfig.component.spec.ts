import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentconfigComponent } from './componentconfig.component';

describe('ComponentconfigComponent', () => {
  let component: ComponentconfigComponent;
  let fixture: ComponentFixture<ComponentconfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentconfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
