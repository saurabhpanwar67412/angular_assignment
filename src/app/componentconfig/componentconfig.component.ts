import { Component, OnInit , ComponentFactoryResolver, ViewContainerRef, ViewChild,ComponentRef,ViewRef} from '@angular/core';
import { ChildcomponentComponent } from "../childcomponent/childcomponent.component";

@Component({
  selector: 'app-componentconfig',
  templateUrl: './componentconfig.component.html',
  styleUrls: ['./componentconfig.component.scss']
})
export class ComponentconfigComponent implements OnInit {
  @ViewChild("viewContainerRef", { read: ViewContainerRef })
  VCR: ViewContainerRef;
  child_unique_key: number = 0;
  componentsReferences = Array<ComponentRef<ChildcomponentComponent>>()
  

  constructor(private CFR: ComponentFactoryResolver) { }

  ngOnInit(): void {
  }
  createComponent() {
    let componentFactory = this.CFR.resolveComponentFactory(ChildcomponentComponent);

    let childComponentRef = this.VCR.createComponent(componentFactory);

    let childComponent = childComponentRef.instance;
    childComponent.unique_key = ++this.child_unique_key;
    childComponent.parentRef = this;

    // add reference for newly created component
    this.componentsReferences.push(childComponentRef);
  }

  
  remove(key: number) {
    if (this.VCR.length < 1) return;
debugger;
    let componentRef = this.componentsReferences.filter(
      x => x.instance.unique_key == key
    )[0];

    let vcrIndex: number = this.VCR.indexOf(componentRef as any);

    // removing component from container
    this.VCR.remove(vcrIndex);

    // removing component from the list
    this.componentsReferences = this.componentsReferences.filter(
      x => x.instance.unique_key !== key
    );
  }
  

}
