import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {ComponentconfigComponent} from '../componentconfig/componentconfig.component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  public unique_key: number;
  public parentRef: ComponentconfigComponent;

  constructor(public dialogRef: MatDialogRef<ModalComponent>) { }

  ngOnInit(): void {
  }

  donothing() {
    alert("you didn't delete anything");
    this.closeModal();
  }

  // If the user clicks the cancel button a.k.a. the go back button, then\
  // just close the modal
  closeModal() {
    this.dialogRef.close();
  }
  remove_me() {
    console.log(this.unique_key)
    this.parentRef.remove(this.unique_key)
    this.dialogRef.close();
  }

}
